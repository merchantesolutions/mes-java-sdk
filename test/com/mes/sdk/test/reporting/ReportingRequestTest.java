package com.mes.sdk.test.reporting;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import com.mes.sdk.reporting.ReportingRequest;
import com.mes.sdk.reporting.ReportingRequest.ReportMode;
import com.mes.sdk.reporting.ReportingRequest.ReportType;

public class ReportingRequestTest {
	
	@Test
	public void setStartDate() {
		ReportingRequest request = new ReportingRequest(ReportType.ACHRETURN, ReportMode.DETAIL);
		request.setStartDate("01", "31", "1970");
		
		assertEquals(request.requestTable.get("reportDateBegin"), "01/31/1970");
	}

	@Test
	public void setEndDate() {
		ReportingRequest request = new ReportingRequest(ReportType.ACHRETURN, ReportMode.DETAIL);
		request.setEndDate("01", "31", "1970");
		
		assertEquals(request.requestTable.get("reportDateEnd"), "01/31/1970");
	}
}
